#!/bin/bash
# (C) 2016, Yann E. MORIN <yann.morin.1998@free.fr>
# License: at your option, either one of: GPLv2 or GPLv3.
# See LICENSE.GPLv2 and LICENSE.GPLv3 in this repository.

gw_rename() {
    local opts
    local old_branch new_branch old_tag new_tag base cover version

    opts="$( getopt -n "${my_name}" -o h -l help -- "${@}" )"
    eval set -- "${opts}"

    while [ ${#} -gt 0 ]; do
        case "${1}" in
        (-h|--help) gw_show_help rename; exit 0;;
        (--)        shift; break;;
        esac
    done

    if [ ${#} -ne 2 ]; then
        error "usage: git br rename <old-branch-name> <new-branch-name>\n"
    fi
    old_branch="${1}"
    new_branch="${2}"

    old_tag="base/${old_branch}"
    new_tag="base/${new_branch}"

    if gw_branch_has_base "${old_branch}"; then
        base="$( gw_branch_get_base "${old_branch}" )"
        gw_new -b "${base}" "${new_branch}"
    else
        gw_new -b "${old_tag}" "${new_branch}"
        gw_branch_set_base "${new_branch}"
    fi
    git cherry-pick "${old_tag}..${old_branch}"
    if gw_branch_has_cover "${old_branch}"; then
        gw_branch_get_cover "${old_branch}" \
        |gw_branch_set_cover "${new_branch}"
    fi
    version="$( gw_get_note "${old_tag}" "version" )"
    if [ -n "${version}" ]; then
        gw_set_note "${new_tag}" "version" "${1}"
    fi
    gw_remove "${old_branch}"
}

gw_rename_help() {
    cat <<_EOF_
git-br rename:
    rename a topical branch.

    See 'git br -h' for more information about git-br.
_EOF_
}

#-----------------------
# vim: ft=sh tabstop=4
