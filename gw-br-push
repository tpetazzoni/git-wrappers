#!/bin/bash
# (C) 2014, Yann E. MORIN <yann.morin.1998@free.fr>
# License: at your option, either one of: GPLv2 or GPLv3,
# see LICENSE.GPLv2 and LICENSE.GPLv3 in this repository

gw_push() {
    local opts all
    local -a remotes
    local -a branches
    local r b t

    opts="$( getopt -n "${my_name}" -o ha -l help,all -- "${@}" )"
    eval set -- "${opts}"

    all=n
    while [ ${#} -gt 0 ]; do
        case "${1}" in
        (-h|--help)     gw_show_help push; exit 0;;
        (-a|--all)      all=y; shift;;
        (--)            shift; break;;
        esac
    done

    if [ "${all}" = "y" ]; then
        # Push all topical branches and master
        branches=( master $( gw_branches -s ) )
    elif [ ${#} -gt 0 ]; then
        branches=$( "${@}" )
    else
        b="$( git rev-parse --abbrev-ref HEAD )"
        if [ "${b}" = "HEAD" ]; then
            error 'not on a branch.\n'
        fi
        branches=( "${b}" )
    fi

    for b in "${branches[@]}"; do
        # Only accept topical branches and master
        if [ "${b}" = "master" ]; then
            continue
        fi
        if ! gw_is_topical_branch "${b}"; then
            error "branch %s is not a topical branch.\n" "${b}"
        fi
    done

    remotes=( $( git remote -v \
                 |awk '$(NF)=="(push)" && $(2)!="/dev/null" {print $(1);}'
               )
            )

    for r in "${remotes[@]}"; do
        printf "Pushing branches to '%s'...\n" "${r}"
        git push -f "${r}" "${branches[@]}"
    done
}

gw_push_help() {
    cat <<_EOF_
git-br push:
    Push the current branch to all remotes that have a push-url that is
    not set to /dev/null.

    See 'git br -h' for more information about git-br.
_EOF_
}

#-----------------------
# vim: ft=sh tabstop=4
