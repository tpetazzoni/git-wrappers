#!/bin/bash
# (C) 2016, Yann E. MORIN <yann.morin.1998@free.fr>
# License: at your option, either one of: GPLv2 or GPLv3.
# See LICENSE.GPLv2 and LICENSE.GPLv3 in this repository.

gw_new() {
    local opts
    local base branch tag add_cover

    opts="$( getopt -n "${my_name}" -o hb:c -l help,base:,cover -- "${@}" )"
    eval set -- "${opts}"

    add_cover=n
    while [ ${#} -gt 0 ]; do
        case "${1}" in
        (-h|--help)     gw_show_help new; exit 0;;
        (-c|--cover)    add_cover=y; shift;;
        (-b|--base)     base="${2}"; shift 2;;
        (--)            shift; break;;
        esac
    done

    if [ $# -ne 1 ]; then
        error 'usage: git br new [-c] [-b base] branch-name\n'
        exit 1
    fi

    branch="${1}"
    tag="base/${branch}"

    if [ -n "${base}" ]; then
        debug 'checking if base branch %s exists\n' "${base}"
        if ! gw_check_object "${base}"; then
            error 'base branch '%s' does not exists\n' "${base}"
        fi
    fi
    debug 'checking if branch %s already exists\n' "${branch}"
    if gw_check_object "${branch}"; then
        error 'branch '%s' already exists\n'  "${branch}"
    fi
    debug 'checking if base tag %s already exists\n' "${tag}"
    if gw_check_object "${tag}" >/dev/null 2>&1; then
        error "base tag '%s' already exists\n" "${tag}"
    fi

    debug 'creating tag %s\n' "${tag}"
    git tag -m "Tag for base of branch '${branch}'" "${tag}" ${base}

    debug 'creating branch %s\n' "${branch}"
    git checkout -b "${branch}" "${tag}"

    if [ -n "${base}" ]; then
        debug 'annotating tag\n'
        git notes add -m "onto: ${base}" "${tag}" >/dev/null 2>&1
    fi

    if [ "${add_cover}" = "y" ]; then
        gw_cover -e "${branch}"
    fi
}

gw_new_help() {
    cat <<_EOF_
git-br new:
    Create a new topical branch.

    See 'git br -h' for more information.
_EOF_
}

#-----------------------
# vim: ft=sh tabstop=4
