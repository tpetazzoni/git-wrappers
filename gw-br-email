#!/bin/bash
# (C) 2012..2016, "Yann E. MORIN" <yann.morin.1998@free.fr>
# License: at your option, either one of: GPLv2 or GPLv3,
# see LICENSE.GPLv2 and LICENSE.GPLv3 in this repository

gw_email() {
    local opts
    local o O
    local -a email_opts
    local dry_run force series ignore_pw pull version cover_cc_all
    local subject_suffix cover_subject remote url branch tag nb_wip
    local -a to cc bcc cover_to cover_cc cover_bcc _to _cc _bcc
    local _t1 _t2 _t3
    local patch_dir smtp_server smtp_passwd cover_body abort in_reply_to

    o="h"
    O="help,dry-run,force,version:,series,no-series,pull,not-pull"
    O+=",patchwork,no-patchwork,subject-suffix:,cover-subject:"
    O+=",to:,cc:,bcc:,cover-to:,cover-cc:,cover-bcc:,cover-cc-all"
    O+=",in-reply-to:"
    opts="$( getopt -n "${my_name}" -o "${o}" -l "${O}" -- "${@}" )"
    eval set -- "${opts}"

    force=false
    dry_run=false
    version=
    series="$( gw_get_config "br.email-send-series" true )"
    cover_cc_all="$( gw_get_config "br.email-cover-cc-all" true )"
    pull="$( gw_get_config "br.email-request-pull" false )"
    ignore_pw="$( gw_get_config "br.email-ignore-patchwork" false )"
    while [ ${#} -gt 0 ]; do
        case "${1}" in
        (-h|--help)         gw_show_help email; exit 0;;
        (--dry-run)         dry_run=true; shift;;
        (--force)           force=true; shift;;
        (--version)         version="${2}"; shift 2;;
        (--series)          series=true; shift;;
        (--no-series)       series=false; shift;;
        (--pull)            pull=true; shift;;
        (--not-pull)        pull=false; shift;;
        (--patchwork)       ignore_pw=false; shift;;
        (--no-patchwork)    ignore_pw=true; shift;;
        (--to)              to+=( "${2}" ); shift 2;;
        (--cc)              cc+=( "${2}" ); shift 2;;
        (--bcc)             bcc+=( "${2}" ); shift 2;;
        (--cover-to)        cover_to+=( "${2}" ); shift 2;;
        (--cover-cc)        cover_cc+=( "${2}" ); shift 2;;
        (--cover-bcc)       cover_bcc+=( "${2}" ); shift 2;;
        (--cover-cc-all)    cover_cc_all=true; shift;;
        (--no-cover-cc-all) cover_cc_all=false; shift;;
        (--cover-subject)   cover_subject="${2}"; shift 2;;
        (--subject-suffix)  subject_suffix="${2}"; shift 2;;
        (--in-reply-to)     in_reply_to="${2}"; shift 2;;
        (--)                shift; break;;
        esac
    done

    if ${force} && ${dry_run}; then
        error 'cannot use --force and --dry-run together.\n'
    fi
    if [ ${#to[@]} -eq 0 ]; then
        error 'no "to:" recipient specified.\n'
    fi

    case "${#}" in
    (0)     error 'no remote specified.\n';;
    (1)     remote="${1}";;
    (2)     remote="${1}"; branch="${2}";;
    (*)     error 'too many arguments.\n'
    esac
    if [ -z "${branch}" ]; then
        branch="$( git rev-parse --abbrev-ref HEAD )"
        if [ "${branch}" = "HEAD" ]; then
            error 'cannot send email in detached state.\n'
        fi
    fi
    if ! gw_is_topical_branch "${branch}"; then
        error 'cannot send email for non-topical branch %s\n' "${branch}"
    fi
    tag="base/${branch}"

    # Don't accept to email a branch that has a commit marked as [WIP]
    # or TEST-ME (for gitlab-ci)
    nb_wip=$( gw_log --oneline "${branch}" \
              |sed -r -e 's/^[^[:space:]]+[[:space:]]+//;' \
                      -e '/^(\[WIP\]|WIP:|TEST.ME)/!d' \
              |wc -l
            )
    if [ ${nb_wip} -ne 0 ]; then
        error 'cannot email branch %s with %d WIP or TEST-ME commit(s)\n' "${branch}" ${nb_wip}
    fi

    # We can only accept remotes that are publicly reachable, which
    # means only remotes in publicly useable protocols.
    case "${remote}" in
    (git://*|http://*|https://*)
        url="${remote}"
        ;;
    (*)
        url="$( git remote show -n "${remote}" 2>/dev/null \
                |sed -r -e '/^[[:space:]]*Fetch URL: (.+)$/!d; s//\1/;'
              )"
        case "${url}" in
        (git://*|http://*|https://*)    ;; # OK, valid.
        (*)
            if ${pull}; then
                error 'remote %s is not a publicly accesible repository.\n' "${remote}"
            fi
            warn 'remote %s is not a publicly accesible repository.\n' "${remote}"
            ;;
        esac
        ;;
    esac

    if ${pull}; then
        if ! git ls-remote --heads "${url}" >/dev/null 2>&1; then
            error 'remote %s is not reachable.\n' "${remote}"
        fi
        _t1="$( git ls-remote --heads "${url}" "${branch}" )"
        if [ -z "${_t1}" ]; then
            error 'topical branch %s is not present on remote %s.\n' \
                  "${branch}" "${remote}"
        fi
        _t2="$( git log -n 1 --pretty=tformat:%H "${branch}" )"
        _t3="$( sed -r -e 's/[[:space:]].+//;' <<<"${_t1}" )"
        if [ "${_t2}" != "${_t3}" ]; then
            warn 'topical branch %s is not up-to-date on remote %s:\n' \
                 "${branch}" "${remote}"
            warn 'local sha1 is %s, remote sha1 is %s.\n' "${_t2}" "${_t3}"
            if ! ${force}; then
                error 'aborting.\n'
            fi
        fi
    fi

    if gw_branch_has_cover "${branch}"; then
        if [ -z "${cover_subject}" ]; then
            cover_subject="$( gw_branch_get_cover "${branch}" |head -n 1 )"
        fi
        cover_body="$( gw_branch_get_cover "${branch}" |tail -n +3 )"
    fi

    if [ -z "${cover_subject}" ]; then
        if ${pull}; then
            cover_subject="Pull request for branch ${branch}"
        else
            read -p "Subject for cover-letter: " cover_subject
            if [ -z "${cover_subject}" ]; then
                error 'you must provide a subject for the cover-letter.\n'
            fi
            cover_subject="${cover_subject} (branch ${branch})"
        fi
    else
        cover_subject="${cover_subject} (branch ${branch})"
    fi

    if [ -z "${version}" ]; then
        version="$( gw_get_note "${tag}" "version" )"
    fi

    patch_dir="$( mktemp -d "${git_dir}/git-br-email.patches.XXXXXX" )"
    git format-patch -M -n --cover-letter --thread \
                     -o "${patch_dir}" "${tag}..${branch}" >/dev/null

    {
        printf '# vim: ft=mail\n'
        printf "# Lines starting with # are ignored.\n"
        printf "# If you want to abort, just save an empty message,\n"
        printf "# or just leave it as-is, without modifications.\n"

        awk '$1=="Subject:" { exit; }
             { print; }
            ' "${patch_dir}/0000-cover-letter.patch"

        printf "Subject: ["
        if ${pull}; then
            printf "pull request"
        else
            _t1=$( git log --oneline "${tag}..${branch}" |wc -l )
            printf "PATCH %0*d/%d" ${#_t1} 0 ${_t1}
        fi
        printf "] %s\n\n" "${cover_subject}"

        printf "Hello All!\n\n"

        if [ -n "${cover_body}" ]; then
            printf '%s\n\n' "${cover_body}"
        else
            printf '\n'
            # This needs to be in two tests, in case there is no version.
            if [ -n "${version}" ] && [ ${version} -gt 1 ]; then
                printf "\nChanges v%d -> v%d:\n  - \n\n" $((version-1)) ${version}
            fi
        fi
        printf "\n"

        _t1="$( git config --get user.name )"
        printf "Regards,\n%s.\n\n\n" "${_t1}"

        # We can't use git-request-pull, it does not like tags and branches
        # that have a slash '/' in their name.
        printf "The following changes since commit "
        git show --no-patch --pretty=tformat:'%H%n%n  %s (%ci)%n%n' "${tag}^{commit}"
        printf "are available in the git repository at:\n\n  %s\n\n" "${url}"
        printf "for you to fetch changes up to "
        git show --no-patch --pretty=tformat:'%H%n%n  %s (%ci)%n%n' "${branch}^{commit}"
        printf -- "----------------------------------------------------------------\n"
        git shortlog "${tag}..${branch}"
        git diff --stat --summary "${tag}..${branch}"

        printf "\n%s \n" "--"
        if [ -f "${HOME}/.signature" ]; then
            cat "${HOME}/.signature"
        fi
    } >"${patch_dir}/0000-cover-letter"
    rm -f "${patch_dir}/0000-cover-letter.patch"

    if [ -n "${subject_suffix}" ]; then
        sed -r -i -e 's/^(Subject: \[[^]]+)]/\1 '"${subject_suffix}"']/' \
            "${patch_dir}/"*
    fi
    if [ -n "${version}" ]; then
        sed -r -i -e 's/^(Subject: \[[^]]+)]/\1 v'"${version}"']/' \
            "${patch_dir}/"*
    fi

    if ${ignore_pw}; then
        sed -r -i -e '/^From:/s/^/X-Patchwork-Hint: ignore\n/' "${patch_dir}/"*
    elif ! ${pull}; then
        sed -r -i -e '/^From:/s/^/X-Patchwork-Hint: ignore\n/' "${patch_dir}/0000-cover-letter"
    fi

    _t1="$( sha1sum "${patch_dir}/0000-cover-letter" |cut -d' ' -f1 )"
    ${EDITOR:-vi} "${patch_dir}/0000-cover-letter"
    _t2="$( sha1sum "${patch_dir}/0000-cover-letter" |cut -d' ' -f1 )"

    abort=false
    if [ ! -s "${patch_dir}/0000-cover-letter" ]; then
        abort=true
    fi
    if [ "${_t1}" = "${_t2}" ]; then
        if ! gw_branch_has_cover "${branch}"; then
            abort=true
        fi
    fi
    if ${abort}; then
        rm -rf "${patch_dir}"
        error 'empty or unmodified cover-letter. Aborting.\n'
    fi

    sed -r -i -e '/^#/d;' "${patch_dir}/0000-cover-letter"

    if ${cover_cc_all}; then
        eval cover_cc+=\( $( sed -r -e '/^(Signed-off-by|Reported-by|CC|Cc): (.*)$/!d;' \
                                    -e 's//\x27\2\x27/;' "${patch_dir}/"*.patch \
                             |sort -u \
                           ) \
                       \)
    fi

    smtp_server="$( git config --get sendemail.smtpserver )"
    if [ -z "${smtp_server}" ]; then
        error 'no smtp server configured.\n'
    fi
    smtp_passwd="$( git config --get sendemail.smtppass || true )"
    if [ -z "${smtp_passwd}" ]; then
        read -s -p "SMTP password for ${smtp_server}: " smtp_passwd
        printf "\n" # 'read -s' does not echo the \n at the end
                    # of the prompt, so we need to do it ourselves.
        if [ -z "${smtp_passwd}" ]; then
            error 'no smtp passord provided.\n'
        fi
    fi

    # Prepare the options for git-send-email for the cover letter.
    email_opts=( --smtp-pass="${smtp_passwd}" )
    if ${dry_run}; then
        email_opts+=( --dry-run )
    fi
    for _t1 in "${cover_to[@]}" "${to[@]}"; do
        email_opts+=( --to "${_t1}" )
    done
    for _t1 in "${cover_cc[@]}" "${cc[@]}"; do
        email_opts+=( --cc "${_t1}" )
    done
    for _t1 in "${cover_bcc[@]}" "${bcc[@]}"; do
        email_opts+=( --bcc "${_t1}" )
    done
    if [ -n "${in_reply_to}" ]; then
        email_opts+=( --in-reply-to "${in_reply_to}" )
    fi
    git send-email "${email_opts[@]}" "${patch_dir}/0000-cover-letter"

    if ${series}; then
        # git always sleeps a bit between two mails, to avoid
        # being caught as a spammer. Since our cover-letter and
        # series are sent with two different git invocations,
        # we're missing a sleep in-between the cover-letter and
        # the first patch.
        sleep 1
        # Prepare the options for git-send-email for the patches.
        email_opts=( --smtp-pass="${smtp_passwd}" )
        if ${dry_run}; then
            email_opts+=( --dry-run )
        fi
        for _t1 in "${to[@]}"; do
            email_opts+=( --to "${_t1}" )
        done
        for _t1 in "${cc[@]}"; do
            email_opts+=( --cc "${_t1}" )
        done
        for _t1 in "${bcc[@]}"; do
            email_opts+=( --bcc "${_t1}" )
        done
        git send-email --no-thread "${email_opts[@]}" "${patch_dir}/"*.patch
    fi

    rm -rf "${patch_dir}"
}

gw_email_help() {
    cat <<_EOF_
git-br email:
    Send a patch series by email.
_EOF_
}
