#!/bin/bash
# (C) 2014, Yann E. MORIN <yann.morin.1998@free.fr>
# License: at your option, either one of: GPLv2 or GPLv3,
# see LICENSE.GPLv2 and LICENSE.GPLv3 in this repository

gw_remove() {
    local opts
    local force branch tag r
    local -a remotes

    opts="$( getopt -n "${my_name}" -o hf -l help,force -- "${@}" )"
    eval set -- "${opts}"

    force=false
    while [ ${#} -gt 0 ]; do
        case "${1}" in
        (-h|--help) gw_show_help remove; exit 0;;
        (-f|--force) force=true; shift;;
        (--)        shift; break;;
        esac
    done

    if [ ${#} -ne 1 ]; then
        error "usage: git br remove branch-name\n"
    fi
    branch="${1}"
    tag="base/${branch}"

    if ! gw_check_object "${branch}"; then
        error 'no branch named %s\n' "${branch}"
    fi
    if ! gw_check_object "${tag}"; then
        error 'no base tag for branch %s\n' "${branch}"
    fi

    remotes=( $( git remote -v \
                 |awk '$(NF)=="(push)" && $(2)!="/dev/null" {print $(1);}'
               )
            )
    for r in "${remotes[@]}"; do
        if ${force} || git remote show -n "${r}" |grep -q -E "^    ${branch}\$"; then
            git push -f "${r}" ":${branch}" || ${force}
        fi
    done
    git branch -D "${branch}"
    git notes remove "${tag}" 2>/dev/null || true
    git tag -d "${tag}" || true
}

gw_remove_help() {
    cat <<_EOF_
git-br remove:
    Delete the specified topical branch, and delete it from all remotes
    that have a push-url not set to /dev/null.
_EOF_
}

#-----------------------
# vim: ft=sh tabstop=4
