#!/bin/bash
# (C) 2016, Yann E. MORIN <yann.morin.1998@free.fr>
# License: at your option, either one of: GPLv2 or GPLv3.
# See LICENSE.GPLv2 and LICENSE.GPLv3 in this repository.

#-------------------------------------------------------------------------------
main() {
    local wrappr cmd git_dir gw

    wrapper="${my_name#git-}"

    if [ $# -ge 1 ]; then
        cmd="${1}"
        shift
    fi

    git_dir="$( cd "$( git rev-parse --git-dir 2>/dev/null )" >/dev/null 2>&1 && pwd )"

    for gw in "${my_dir}/gw-${wrapper}-"*; do
        if [ -f "${gw}" ]; then
            . "${gw}"
        fi
    done

    case "${wrapper}" in
    (br)
        case "${cmd}" in
        (-h|--help)     gw_show_help br; exit 0;;
        (""|list|ls)    gw_list "${@}";;
        (-t|--tree)     gw_list --tree "${@}";;
        (-l|--late)     gw_list --late "${@}";;
        (new)           gw_new "${@}";;
        (remove|rm)     gw_remove "${@}";;
        (log)           gw_log "${@}";;
        (empty)         gw_empty "${@}";;
        (set-base)      gw_set_base "${@}";;
        (push)          gw_push "${@}";;
        (rebase)        gw_rebase "${@}";;
        (cover)         gw_cover "${@}";;
        (email)         gw_email "${@}";;
        (version)       gw_version "${@}";;
        (rename|mv)     gw_rename "${@}";;
        (test)          cmd="${1}"; shift; ${cmd} "${@}";;
        (*)             error "no such command '%s'\n" "${cmd}";;
        esac
        ;;
    (pw)
        case "${cmd}" in
        (-h|--help)     gw_show_help pw; exit 0;;
        (""|pwc)        gw_pwc "${@}";;
        # Special case to use pwc without naming it, as it is the default command
        (*)             gw_pwc "${cmd}" "${@}";;
        esac
        ;;
    (*)
        error "unknown wrapper '%s'\n" "${wrapper}"
        ;;
    esac
}

gw_br_help() {
    cat "${my_dir}/README.br"
}

gw_pw_help() {
    cat "${my_dir}/README.pw"
}

my_name="${0##*/}"
my_dir="$(readlink -e "${0}" |sed -r -e 's:/[^/]+$::' )"
. "${my_dir}/functions"

main "${@}"

#-----------------------
# vim: ft=sh tabstop=4
