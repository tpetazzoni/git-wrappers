#!/bin/bash
# (C) 2014, Yann E. MORIN <yann.morin.1998@free.fr>
# License: at your option, either one of: GPLv2 or GPLv3,
# see LICENSE.GPLv2 and LICENSE.GPLv3 in this repository

gw_rebase() {
    local opts o O
    local -a rebase_opts
    local b all interactive

    o="haix:"
    O="help,all,abort,continue,skip,interactive,exec:"
    opts="$( getopt -n "${my_name}" -o "${o}" -l "${O}" -- "${@}" )"
    eval set -- "${opts}"

    all=false
    interactive=false
    while [ ${#} -gt 0 ]; do
        case "${1}" in
        (-h|--help)     gw_show_help rebase; exit 0;;
        (-a|--all)      all=true; shift;;
        (-i|--interactive)
            rebase_opts+=( --interactive )
            interactive=true
            shift
            ;;
        (--abort|--continue|--skip)
            rebase_opts+=( "${1}" )
            shift
            ;;
        (-x|--exec)
            rebase_opts+=( --exec "${2}" )
            shift 2
            ;;
        (--)            shift; break;;
        esac
    done

    if ${all} && ${interactive}; then
        error "cannot be interactive when rebasing all banches"
    fi

    if ${all}; then
        gw_rebase_all "${@}"
    else
        gw_rebase_one "${rebase_opts[@]}" "${@}"
    fi
}

gw_rebase_one() {
    local opts O
    local mode base branch tag exec_cmd
    local status_file notes_file msg ret

    status_file="${git_dir}/rebase.status"
    notes_file="${git_dir}/rebase.notes"

    O="interactive,abort,continue,skip,exec:"
    opts="$( getopt -n "${my_name}" -o '' -l "${O}" -- "${@}" )"
    eval set -- "${opts}"

    while [ ${#} -gt 0 ]; do
        case "${1}" in
        (-h|--help)     gw_show_help gw_rebase_help; exit 0;;
        (--interactive) mode="interactive"; shift;;
        (--abort)       mode="abort"; shift;;
        (--continue)    mode="continue"; shift;;
        (--skip)        mode="skip"; shift;;
        (--exec)        exec_cmd="${2}"; shift 2;;
        (--)            shift; break;;
        esac
    done
    : "${mode:=rebase}"

    if [ -n "${exec_cmd}" -a "${mode}" != "interactive" ]; then
        error 'cannot specify --exec in non-interactive mode "%s"\n' "${mode}"
    fi

    case "${mode}" in
    (rebase|interactive)
        if [ -f "${status_file}" ]; then
            mode="$( sed -r -e '/^mode=(.+)$/!d; s//\1/;' "${status_file}" )"
            base="$( sed -r -e '/^base=(.+)$/!d; s//\1/;' "${status_file}" )"
            branch="$( sed -r -e '/^branch=(.+)$/!d; s//\1/;' "${status_file}" )"
            error 'rebase already in progress for: %s onto %s\n' \
                  "${branch}" "${base}"
        fi

        if [ ${#} -eq 1 ]; then
            branch="${1}"
        else
            branch="$( git rev-parse --abbrev-ref HEAD )"
            if [ "${branch}" = "HEAD" ]; then
                error 'not rebasing when in detached HEAD state.\n'
            fi
        fi
        tag="base/${branch}"

        debug 'checking if %s is a topical branch.\n' "${branch}"
        if ! gw_is_topical_branch "${branch}"; then
            error 'not rebasing non-topical branch %s.\n' "${branch}"
        fi
        if [ "${mode}" = "rebase" ]; then
            debug 'checking if %s has a base.\n' "${branch}"
            if ! gw_branch_has_base "${branch}"; then
                error 'not rebasing topical branch %s which has no base\n' "${branch}"
            fi
            base="$( gw_branch_get_base "${branch}" )"
            debug "checking if base %s exists\n" "${base}"
            if ! gw_check_object "${base}"; then
                error 'unknown base %s\n' "${base}"
            fi
        fi

        debug 'storing {mode,base,branch} and {notes} in status files\n'
        printf "mode=%s\nbase=%s\nbranch=%s\n" \
               "${mode}" "${base}" "${branch}" \
               >"${status_file}"
        git notes show "${tag}" >"${notes_file}" 2>/dev/null || true

        case "${mode}" in
        (rebase)
            debug 'rebasing %s..%s onto %s\n' "${tag}" "${branch}" "${base}"
            git rebase --onto "${base}" "${tag}" "${branch}"
            ;;
        (interactive)
            debug "starting interactive rebase %s..%s\n" "${tag}" "${branch}"
            git rebase -i ${exec_cmd:+-x "${exec_cmd}"} "${tag}" || {
                ret=${?}
                if [ -d "${git_dir}/rebase-merge/" ]; then
                    debug "error in the middle of interactive rebase (conflict?)\n"
                    exit ${ret}
                else
                    debug "error in the middle of interactive rebase (nothing to do?)\n"
                    rm -f "${status_file}" "${notes_file}"
                    exit ${ret}
                fi
            }
            ;;
        esac
        ;;
    (continue)
        if [ ! -f "${status_file}" ]; then
            error "no rebase in progress\n"
        fi
        mode="$( sed -r -e '/^mode=(.+)$/!d; s//\1/;' "${status_file}" )"
        base="$( sed -r -e '/^base=(.+)$/!d; s//\1/;' "${status_file}" )"
        branch="$( sed -r -e '/^branch=(.+)$/!d; s//\1/;' "${status_file}" )"
        tag="base/${branch}"
        git rebase --continue
        ;;
    (skip)
        if [ ! -f "${status_file}" ]; then
            error "no rebase in progress\n"
        fi
        mode="$( sed -r -e '/^mode=(.+)$/!d; s//\1/;' "${status_file}" )"
        base="$( sed -r -e '/^base=(.+)$/!d; s//\1/;' "${status_file}" )"
        branch="$( sed -r -e '/^branch=(.+)$/!d; s//\1/;' "${status_file}" )"
        tag="base/${branch}"
        git rebase --skip
        ;;
    (abort)
        rm -f "${status_file}" "${notes_file}"
        git rebase --abort
        # Abort is special: we must not apply the closures, below.
        return 0
        ;;
    esac

    # Only do the closures for a standard (aka non-interactive) rebase
    if [ "${mode}" = "rebase" ]; then
        msg="$( printf 'Tag base for branch "%s"' "${branch}" )"

        debug 're-tagging base %s of %s\n' "${base}" "${branch}"
        git notes remove "${tag}" >/dev/null 2>&1 || true # Don't fail if no note
        git tag -f -a -m "${msg}" "${tag}" "${base}"
        [ ! -s "${notes_file}" ] || git notes add -F "${notes_file}" "${tag}"
    fi

    # If we get up to here durign an interactive rebase, it means that
    # we sucessfully stopped at a commit the user wanted to 'edit'. So
    # we must not remove the status files in this case, so that we can
    # continue/skip/abort the rebase later on. This is the case iff the
    # rebase-merge directory exists; any other case means we must remove
    # the files.
    if [ ! -d "${git_dir}/rebase-merge" ]; then
        debug 'removing status files\n'
        rm -f "${status_file}" "${notes_file}"
    fi
}

gw_rebase_all() {
    local regexp noop loop b cb s
    local -a branches

    regexp="${1}"

    cb="$( git rev-parse --abbrev-ref HEAD )"
    if [ "${cb}" = "HEAD" ]; then
        error 'not rebasing when in detached HEAD state.\n'
    fi
    branches=( $( gw_branches -s |grep -E "${regexp}"
                )
             )

    noop=y; loop=y
    while [ "${loop}" = "y" ]; do
        loop=n

        # Rebase branches
        for b in "${branches[@]}"; do
            s="$( git show --pretty='format:%s' "${b}" )"
            if ! gw_is_topical_branch "${b}"; then
                debug 'not rebasing non-topical branch %s\n' "${branch}"
                continue
            fi
            if ! gw_branch_has_base "${b}"; then
                debug "not rebasing topical branch %s with no base.\n" "${b}"
                continue
            fi
            if gw_branch_is_uptodate "${b}"; then
                debug "not rebasing topical branch %s already rebased.\n" "${b}"
                continue
            fi
            noop=n; loop=y
            gw_rebase_one "${b}"
        done
    done

    if [ "${noop}" = "y" ]; then
        printf "All branches already rebased.\n"
    else
        git checkout "${cb}"
        printf "All branches now rebased.\n"
    fi
}

gw_rebase_help() {
    cat <<_EOF_
git-br rebase:
    Rebase a branch ontop of its base.

    See 'git br -h' for more information about git-br.
_EOF_
}

#-----------------------
# vim: ft=sh tabstop=4
